<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://imsmarketing.ie/
 * @since             1.0.0
 * @package           Imswp
 *
 * @wordpress-plugin
 * Plugin Name:       IMSWP
 * Plugin URI:        https://imsmarketing.ie/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            IMS Marketing
 * Author URI:        https://imsmarketing.ie/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       imswp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'IMSWP_VERSION', '1.0.0' );

// Load Composer dependencies.
require_once __DIR__ . '/vendor/autoload.php';
require 'helper-functions.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-imswp-activator.php
 */
function activate_imswp() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-imswp-activator.php';
	Imswp_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-imswp-deactivator.php
 */
function deactivate_imswp() {
	// require_once plugin_dir_path( __FILE__ ) . 'includes/class-imswp-deactivator.php';
	// Imswp_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_imswp' );
register_deactivation_hook( __FILE__, 'deactivate_imswp' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-imswp.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_imswp() {

	$plugin = new Imswp();
	$plugin->run();

}
run_imswp();
