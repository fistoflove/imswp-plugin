<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */
class Imswp {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Imswp_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'IMSWP_VERSION' ) ) {
			$this->version = IMSWP_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'imswp';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_timber();
		$this->define_theme();
		$this->define_blocks();
		$this->define_fields();
		$this->define_helper();
		$this->create_block_library();
		$this->define_git_getter();
		$this->define_sideload();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Imswp_Loader. Orchestrates the hooks of the plugin.
	 * - Imswp_i18n. Defines internationalization functionality.
	 * - Imswp_Admin. Defines all hooks for the admin area.
	 * - Imswp_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-imswp-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-imswp-public.php';

		/**
		 * The class responsible for orchestrating the actions, filters, and options of the
		 * timber library.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-timber.php';

		/**
		 * The class responsible for orchestrating the actions, and filters of the theme.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-theme.php';

		/**
		 * The class responsible for orchestrating the options of the theme.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-theme-options.php';

		/**
		 * The class responsible for all blocks.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-blocks.php';

		/**
		 * The class responsible for all ACF fields.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-fields.php';

		/**
		 * The class responsible for the primary theme helper.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-helper.php';

		/**
		 * The class responsible for the block library
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-block-library.php';

		/**
		 * The class responsible for the Git Getter
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-git-getter.php';

		/**
		 * The class responsible for the Block Side loader
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-imswp-sideloader.php';

		$this->loader = new Imswp_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Imswp_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Imswp_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Imswp_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Imswp_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Handles all timber functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_timber() {

		$plugin_public = new Imswp_Timber( $this->get_plugin_name(), $this->get_version() );
	}

	/**
	 * Handles all theme functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_theme() {
		$theme_options = new Imswp_ThemeOptions( $this->get_plugin_name(), $this->get_version() );
		$plugin_public = new Imswp_Theme( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_head', $plugin_public, 'theme_favicon' );

		$this->loader->add_action( 'admin_menu', $plugin_public, 'theme_option_pages' );

		$this->loader->add_action( 'admin_init', $plugin_public, 'register_image_cdn_field' );
	}

	/**
	 * Handles all block functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_blocks() {
		$plugin_public = new Imswp_Blocks( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'init', $plugin_public, 'register_blocks' );
	}

	/**
	 * Handles all ACF field functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_fields() {
		$plugin_public = new IMSWP\Helper\Fields( $this->get_plugin_name(), $this->get_version() );
//		$this->loader->add_action( 'init', $plugin_public, 'register_blocks' );
	}

	/**
	 * Primary theme function helper
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_helper() {
		$plugin_public = new IMSWP\Helper\Helper( $this->get_plugin_name(), $this->get_version() );
//		$this->loader->add_action( 'init', $plugin_public, 'register_blocks' );
	}

	/**
	 * Primary theme function helper
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function create_block_library() {
		$plugin_public = new Imswp_BlockLibrary( $this->get_plugin_name(), $this->get_version() );
	}

	/**
	 * Initialize the GitGetter functionality.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_git_getter() {
		$plugin_git_getter = new Imswp_GitGetter( $this->get_plugin_name(), $this->get_version() );
	}

	/**
	 * Initialize the SideLoader functionality.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_sideload() {
		$plugin_sideload = new Imswp_SideLoader( $this->get_plugin_name(), $this->get_version() );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Imswp_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
