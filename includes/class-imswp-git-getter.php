<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */

 use Timber\Timber;
 use Timber\Menu;
/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */
class Imswp_GitGetter {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->init_();
	}

	public function init_() {
		add_action('init', [$this, 'schedule_daily_download']);
		add_action('imswp_daily_gitlab_download', [$this, 'download_gitlab_repository_zip']);
	}

	public function schedule_daily_download() {
		if (!wp_next_scheduled('imswp_daily_gitlab_download')) {
			wp_schedule_event(time(), 'daily', 'imswp_daily_gitlab_download');
		}
	}

	public function download_gitlab_repository_zip() {
		$projects = get_option('gitlab_projects', []);
		if (empty($projects)) {
			error_log('No GitLab projects configured.');
			return ['success' => false];
		}

		$updated_themes = [];
		foreach ($projects as $project) {
			$gitlab_token = $project['token'];
			$project_id = urlencode($project['name']);
			$zip_file_path = WP_CONTENT_DIR . '/uploads/' . sanitize_file_name($project_id) . '.zip';

			if (empty($gitlab_token) || empty($project_id)) {
				error_log('GitLab token or project ID is missing for project: ' . $project_id);
				continue;
			}

			$gitlab_url = "https://gitlab.com/api/v4/projects/{$project_id}/repository/archive.zip";
			$response = wp_remote_get($gitlab_url, [
				'headers' => [
					'Authorization' => 'Bearer ' . $gitlab_token,
				],
			]);

			if (is_wp_error($response)) {
				error_log('GitLab API error for project ' . $project_id . ': ' . $response->get_error_message());
				continue;
			}

			if (wp_remote_retrieve_response_code($response) === 200) {
				$zip_content = wp_remote_retrieve_body($response);
				file_put_contents($zip_file_path, $zip_content);
				error_log('Repository ZIP downloaded successfully for project: ' . $project_id);
				$this->unzip_repository($zip_file_path, $project_id);
				$updated_themes[] = urldecode($project['name']);
			} else {
				error_log('Failed to download repository ZIP for project ' . $project_id . '. HTTP status code: ' . wp_remote_retrieve_response_code($response));
			}
		}
		return ['success' => true, 'updated_themes' => $updated_themes];
	}

	private function unzip_repository($zip_file_path, $project_id) {
		$theme_root = get_theme_root();
		$project_name_parts = explode('%2F', $project_id);
		$project_name = end($project_name_parts);
		$theme_dir = $theme_root . '/' . sanitize_file_name($project_name);

		// Delete any themes containing the project name
		$this->delete_existing_themes($theme_root, $project_name);

		$zip = new ZipArchive;
		if ($zip->open($zip_file_path) === TRUE) {
			$temp_dir = $theme_root . '/temp_' . sanitize_file_name($project_name);
			$zip->extractTo($temp_dir);
			$zip->close();
			error_log('Repository unzipped successfully for project: ' . $project_id);

			// Rename the extracted folder
			$extracted_folder = glob($temp_dir . '/*', GLOB_ONLYDIR)[0];
			rename($extracted_folder, $theme_dir);
			rmdir($temp_dir);
		} else {
			error_log('Failed to unzip repository for project: ' . $project_id);
		}
		unlink($zip_file_path);
	}

	private function delete_existing_themes($theme_root, $project_name) {
		$themes = scandir($theme_root);
		foreach ($themes as $theme) {
			if (strpos($theme, sanitize_file_name($project_name)) !== false) {
				$this->delete_directory($theme_root . '/' . $theme);
			}
		}
	}

	private function delete_directory($dir) {
		if (!file_exists($dir)) {
			return true;
		}

		if (!is_dir($dir)) {
			return unlink($dir);
		}

		foreach (scandir($dir) as $item) {
			if ($item == '.' || $item == '..') {
				continue;
			}

			if (!$this->delete_directory($dir . DIRECTORY_SEPARATOR . $item)) {
				return false;
			}
		}

		return rmdir($dir);
	}
}
