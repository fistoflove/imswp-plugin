<?php

namespace IMSWP\Helper;

use Timber\Timber;

class Fields
{

  protected $block;
  /**
   * The ID of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string    $plugin_name    The ID of this plugin.
   */
  private $plugin_name;

  /**
   * The version of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string    $version    The current version of this plugin.
   */
  private $version;

  /**
   * Initialize the class and set its properties.
   *
   * @since    1.0.0
   * @param      string    $plugin_name       The name of this plugin.
   * @param      string    $version    The version of this plugin.
   */

  const PREFIX_DELIMITER = '_';

  const FIELD_TYPE_TAB = 'tab';
  const FIELD_TYPE_GROUP = 'group';
  const FIELD_TYPE_REPEATER = 'repeater';
  const FIELD_TYPE_CLONE = 'clone';

  public function __construct($path)
  {
    $this->setBlock($path);
  }

  public function setBlock($path)
  {
    $pathArray = explode("/", $path);
    $this->block = end($pathArray);
  }

  public function sanitize_title($title, $s)
  {
    return strtolower(str_replace(' ', $s, $title));
  }

  public function build_field_key($field, $title)
  {
    return $this->block . self::PREFIX_DELIMITER . $this->sanitize_title($title, self::PREFIX_DELIMITER) . self::PREFIX_DELIMITER . $this->sanitize_title(strtolower($field[0]), self::PREFIX_DELIMITER);
  }

  public function build_content_tab($title, $fields, $prefix_fields)
  {
    $data = [
      'key' => 'group' . $this->block . $this->sanitize_title($title, self::PREFIX_DELIMITER),
      'title' => $title,
      'fields' => $this->build_fields($title, $fields, $prefix_fields),
      'location' => $this->build_location_array(),
    ];
    return $data;
  }

  public function build_fields($title, $fields, $prefix_fields)
  {
    $tabFieldKey = 'field_tab_' . $this->block . self::PREFIX_DELIMITER . $this->sanitize_title($title, self::PREFIX_DELIMITER);

    $field_data = [
      [
        'key' => $tabFieldKey,
        'label' => $title,
        'name' => '',
        'type' => self::FIELD_TYPE_TAB
      ],
      [
        'key' => 'field_group_' . $this->block . self::PREFIX_DELIMITER . $this->sanitize_title($title, self::PREFIX_DELIMITER),
        'label' => $title,
        'name' => $this->sanitize_title($title, self::PREFIX_DELIMITER),
        'type' => self::FIELD_TYPE_GROUP,
        'sub_fields' => []
      ]
    ];

    foreach ($fields as $field) {
      $fieldKey = $this->build_field_key($field, $title);
      $fieldName = $this->sanitize_title($field[0], self::PREFIX_DELIMITER) . ($prefix_fields ? self::PREFIX_DELIMITER . $this->sanitize_title($title, self::PREFIX_DELIMITER) : '');

      $subField = [
        'key' => $fieldKey,
        'label' => $field[0],
        'name' => $fieldName,
        'type' => $field[1]
      ];

      if ($field[1] == self::FIELD_TYPE_REPEATER) {
        $subField['sub_fields'] = $this->build_subfields($this->sanitize_title($field[0], self::PREFIX_DELIMITER), $field[2], $title);
      } elseif ($field[1] == self::FIELD_TYPE_CLONE) {
        $subField['clone'] = [0 => $field[2]];
      } elseif ($field[1] == 'select') {
        $subField['choices'] = $field[2];
      }

      $field_data[1]['sub_fields'][] = $subField;
    }

    return $field_data;
  }

  public function build_subfields($parent, $subfields, $title)
  {
    $subfield_data = [];

    foreach ($subfields as $subfield) {
      $fieldKey = $this->build_field_key($subfield, $title);
      $fieldName = $this->sanitize_title($subfield[0], self::PREFIX_DELIMITER);

      $subField = [
        'key' => $fieldKey,
        'label' => $subfield[0],
        'name' => $fieldName,
        'type' => $subfield[1]
      ];

      if ($subfield[1] == self::FIELD_TYPE_CLONE) {
        $subField['clone'] = [0 => $subfield[2]];
      } elseif ($subfield[1] == 'select') {
        $subField['choices'] = $subfield[2];
      }

      $subfield_data[] = $subField;
    }

    return $subfield_data;
  }

  public function build_location_array()
  {
    return [
      [
        [
          'param' => 'block',
          'operator' => '==',
          'value' => 'acf/' . $this->block,
        ],
      ],
    ];
  }

  public function register_fields($data)
  {
    if (function_exists('acf_add_local_field_group')) {
      acf_add_local_field_group($data);
    }
  }

  public function register_tab($name, $data, $prefix = false)
  {
    $this->register_fields($this->build_content_tab($name, $data, $prefix));
  }
}
