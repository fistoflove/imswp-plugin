<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */

 use Timber\Timber;

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */

class Imswp_BlockLibrary {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->init();
	}

	public function init() {
		add_action('admin_menu', [$this, 'add_admin_page']);
		add_action('admin_enqueue_scripts', [$this, 'enqueue_admin_scripts']);
		add_action('wp_ajax_block_search', [$this, 'handle_block_search']);
		add_action('admin_init', [$this, 'register_settings']);
		add_action('wp_ajax_manual_gitlab_download', [$this, 'handle_manual_gitlab_download']);
	}

	public function enqueue_admin_scripts() {
		wp_enqueue_script('block-searcher-admin-script', get_template_directory_uri() . '/static/block-finder.js', array('jquery'), null, true);
		wp_localize_script('block-searcher-admin-script', 'block_searcher_ajax', array(
			'ajax_url' => admin_url('admin-ajax.php'),
			'nonce'    => wp_create_nonce('block_searcher_nonce'),
		));
	}

	public function handle_block_search() {
		check_ajax_referer('block_searcher_nonce', 'nonce');

		if (isset($_POST['block_slug']) && !empty($_POST['block_slug'])) {
			global $wpdb;
			$block_slug = sanitize_text_field($_POST['block_slug']);
			$query = "
				SELECT ID, post_title 
				FROM $wpdb->posts 
				WHERE post_content REGEXP %s 
				AND post_type = 'page' 
				AND post_status = 'publish'
			";
			$regexp = '[[:<:]]' . $wpdb->esc_like($block_slug) . '[[:>:]]';
			$results = $wpdb->get_results($wpdb->prepare($query, $regexp));

			if ($results) {
				$output = '<h2>Search Results</h2>';
				$output .= '<table class="widefat fixed" cellspacing="0">
							<thead>
								<tr>
									<th scope="col">Page Title</th>
									<th scope="col">Edit Link</th>
									<th scope="col">View Link</th>
								</tr>
							</thead>
							<tbody>';
				foreach ($results as $post) {
					$edit_link = get_edit_post_link($post->ID);
					$view_link = get_permalink($post->ID);
					$output .= '<tr>
								<td>' . esc_html($post->post_title) . '</td>
								<td><a href="' . esc_url($edit_link) . '" target="_blank">Edit</a></td>
								<td><a href="' . esc_url($view_link) . '" target="_blank">View</a></td>
							  </tr>';
				}
				$output .= '</tbody></table>';
			} else {
				$output = '<p>No results found.</p>';
			}
			wp_send_json_success($output);
		} else {
			wp_send_json_error('Invalid block slug.');
		}
	}

	public function render_block_library() {
		$context = Timber::context();
		$context['title'] = 'Block Library';

		$blocks = [];
		$categories = [];
		$keywords = [];
		$supports = [];
		$themes = [];

		$global_mode = get_option('block_library_global_mode', false);

		if ($global_mode) {
			$themes_dir = get_theme_root();
			$theme_dirs = scandir($themes_dir);
			foreach ($theme_dirs as $theme_dir) {
				if ($theme_dir !== '.' && $theme_dir !== '..') {
					$blocks_dir = $themes_dir . '/' . $theme_dir . '/blocks';
					$theme_blocks = [];
					$this->scan_blocks_directory($blocks_dir, $theme_blocks, $categories, $keywords, $supports, $theme_dir);
					if (!empty($theme_blocks)) {
						$blocks = array_merge($blocks, $theme_blocks);
						$themes[] = $theme_dir;
					}
				}
			}
		} else {
			$blocks_dir = get_template_directory() . '/blocks';
			$this->scan_blocks_directory($blocks_dir, $blocks, $categories, $keywords, $supports, wp_get_theme()->get('Name'));
		}

		// Remove duplicate values and sort
		$categories = array_unique($categories);
		sort($categories);
		$keywords = array_unique($keywords);
		sort($keywords);
		$supports = array_unique($supports);
		sort($supports);
		$themes = array_unique($themes);
		sort($themes);

		// Add data to the context
		$context['blocks'] = $blocks;
		$context['filters'] = [
			'categories' => $categories,
			'keywords' => $keywords,
			'supports' => $supports,
			'themes' => $themes
		];

		// Render the template
		Timber::render(plugin_dir_path( __DIR__ ) . 'templates/block-library.twig', $context);
	}

	private function scan_blocks_directory($blocks_dir, &$blocks, &$categories, &$keywords, &$supports, $theme_name) {
		if (is_dir($blocks_dir)) {
			$block_dirs = scandir($blocks_dir);
			foreach ($block_dirs as $block_dir) {
				if ($block_dir !== '.' && $block_dir !== '..') {
					$block_path = $blocks_dir . '/' . $block_dir;
					$block_json_path = $block_path . '/block.json';
					$template_html_path = $block_path . '/template.html';
					$screenshot_path = $block_path . '/screenshot.png';
					if (is_dir($block_path) && file_exists($block_json_path)) {
						$block_json = file_get_contents($block_json_path);
						$block_data = json_decode($block_json, true);
						$template_html = file_exists($template_html_path) ? file_get_contents($template_html_path) : '';
						$screenshot_url = file_exists($screenshot_path) ? get_theme_root_uri() . '/' . $theme_name . '/blocks/' . $block_dir . '/screenshot.png' : '';
						$supports_screenshot = file_exists($screenshot_path) ? 'screenshot' : '';
						$blocks[] = [
							'name' => str_replace('acf/', '', $block_data['name']),
							'title' => $block_data['title'],
							'description' => $block_data['description'],
							'category' => $block_data['category'],
							'keywords' => $block_data['keywords'] ?? [],
							'styles' => $block_data['styles'] ?? [],
							'supports' => array_merge(array_keys($block_data['supports'] ?? []), [$supports_screenshot]),
							'template_html' => $template_html,
							'screenshot_url' => $screenshot_url,
							'theme' => $theme_name
						];
						$categories[] = $block_data['category'];
						if (isset($block_data['keywords']) && is_array($block_data['keywords'])) {
							$keywords = array_merge($keywords, $block_data['keywords']);
						}
						if (isset($block_data['supports']) && is_array($block_data['supports'])) {
							$supports = array_merge($supports, array_keys($block_data['supports']));
							if ($supports_screenshot) {
								$supports[] = 'screenshot';
							}
						}
					}
				}
			}
		}
	}

	public function add_admin_page() {
		add_menu_page(
			'Block Library',                 // Page title
			'Block Library',                 // Menu title
			'manage_options',                // Capability
			'block-library',                 // Menu slug
			[$this, 'render_admin_page'],    // Callback function
			'dashicons-block-default',       // Icon URL (dashicon)
			20                               // Position
		);
		add_submenu_page(
			'block-library',
			'Block Library Settings',
			'Settings',
			'manage_options',
			'block-library-settings',
			[$this, 'render_settings_page']
		);
	}

	public function register_settings() {
		register_setting('block_library_settings', 'block_library_global_mode');
		add_settings_section('block_library_settings_section', 'Block Library Settings', null, 'block-library-settings');
		add_settings_field('block_library_global_mode', 'Global Mode', [$this, 'render_global_mode_field'], 'block-library-settings', 'block_library_settings_section');
		add_settings_section('block_library_gitlab_section', 'GitLab Repository Settings', null, 'block-library-settings');
		add_settings_field('gitlab_projects', 'GitLab Projects', [$this, 'render_gitlab_projects_field'], 'block-library-settings', 'block_library_gitlab_section');
		add_settings_field('manual_download', 'Manual Download', [$this, 'render_manual_download_button'], 'block-library-settings', 'block_library_gitlab_section');
		register_setting('block_library_settings', 'gitlab_projects');
		add_settings_section('block_library_sideload_section', 'Block Side Loading', null, 'block-library-settings');
		add_settings_field('block_sideload_enabled', 'Enable Block Side Loading', [$this, 'render_block_sideload_enabled_field'], 'block-library-settings', 'block_library_sideload_section');
		register_setting('block_library_settings', 'block_sideload_enabled');
	}

	public function render_global_mode_field() {
		$global_mode = get_option('block_library_global_mode', false);
		echo '<input type="checkbox" name="block_library_global_mode" value="1"' . checked(1, $global_mode, false) . '> Enable Global Mode';
	}

	public function render_gitlab_projects_field() {
		$projects = get_option('gitlab_projects', []);
		?>
		<div id="gitlab-projects-wrapper">
			<?php if (!empty($projects)) : ?>
				<?php foreach ($projects as $index => $project) : ?>
					<div class="gitlab-project">
						<input type="text" name="gitlab_projects[<?php echo $index; ?>][name]" value="<?php echo esc_attr($project['name']); ?>" placeholder="Project Name" class="regular-text">
						<input type="text" name="gitlab_projects[<?php echo $index; ?>][token]" value="<?php echo esc_attr($project['token']); ?>" placeholder="Access Token" class="regular-text">
						<button type="button" class="button remove-project">Remove</button>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<button type="button" class="button" id="add-gitlab-project">Add Project</button>
		<script>
			document.addEventListener('DOMContentLoaded', function() {
				const wrapper = document.getElementById('gitlab-projects-wrapper');
				const addButton = document.getElementById('add-gitlab-project');

				addButton.addEventListener('click', function() {
					const index = wrapper.children.length;
					const projectDiv = document.createElement('div');
					projectDiv.classList.add('gitlab-project');
					projectDiv.innerHTML = `
						<input type="text" name="gitlab_projects[${index}][name]" placeholder="Project Name" class="regular-text">
						<input type="text" name="gitlab_projects[${index}][token]" placeholder="Access Token" class="regular-text">
						<button type="button" class="button remove-project">Remove</button>
					`;
					wrapper.appendChild(projectDiv);
				});

				wrapper.addEventListener('click', function(e) {
					if (e.target.classList.contains('remove-project')) {
						e.target.parentElement.remove();
					}
				});
			});
		</script>
		<?php
	}

	public function render_manual_download_button() {
		?>
		<button type="button" class="button" id="manual-download-button">Download Now</button>
		<div id="download-feedback" style="display:none; margin-top: 10px;">
			<p>Downloading repositories, please wait...</p>
		</div>
		<div id="download-summary" style="display:none; margin-top: 10px;">
			<h3>Download Summary</h3>
			<ul id="download-summary-list"></ul>
		</div>
		<div id="download-error" style="display:none; margin-top: 10px; color: red;">
			<p>An error occurred while downloading the repositories.</p>
		</div>
		<script>
			document.addEventListener('DOMContentLoaded', function() {
				const button = document.getElementById('manual-download-button');
				const feedback = document.getElementById('download-feedback');
				const summary = document.getElementById('download-summary');
				const summaryList = document.getElementById('download-summary-list');
				const errorDiv = document.getElementById('download-error');
				button.addEventListener('click', function() {
					feedback.style.display = 'block';
					summary.style.display = 'none';
					errorDiv.style.display = 'none';
					summaryList.innerHTML = '';
					fetch('<?php echo admin_url('admin-ajax.php'); ?>?action=manual_gitlab_download')
						.then(response => response.json())
						.then(data => {
							console.log('Response data:', data); // Log the response data for debugging
							feedback.style.display = 'none';
							if (data.success) {
								summary.style.display = 'block';
								const updatedThemes = data.data.updated_themes;
								if (updatedThemes && updatedThemes.length > 0) {
									updatedThemes.forEach(theme => {
										const listItem = document.createElement('li');
										listItem.textContent = theme;
										summaryList.appendChild(listItem);
									});
								} else {
									const listItem = document.createElement('li');
									listItem.textContent = 'No themes were updated.';
									summaryList.appendChild(listItem);
								}
							} else {
								errorDiv.style.display = 'block';
								errorDiv.textContent = data.message;
							}
						})
						.catch(error => {
							console.error('Fetch error:', error); // Log the fetch error for debugging
							feedback.style.display = 'none';
							errorDiv.style.display = 'block';
							errorDiv.textContent = 'An error occurred: ' + error.message;
						});
				});
			});
		</script>
		<?php
	}

	public function render_block_sideload_enabled_field() {
		$block_sideload_enabled = get_option('block_sideload_enabled', false);
		echo '<input type="checkbox" name="block_sideload_enabled" value="1"' . checked(1, $block_sideload_enabled, false) . '> Enable Block Side Loading';
	}

	public function render_gitlab_project_name_field() {
		$project_name = get_option('gitlab_project_name', '');
		echo '<input type="text" name="gitlab_project_name" value="' . esc_attr($project_name) . '" class="regular-text">';
	}

	public function render_gitlab_access_token_field() {
		$access_token = get_option('gitlab_access_token', '');
		echo '<input type="text" name="gitlab_access_token" value="' . esc_attr($access_token) . '" class="regular-text">';
	}

	public function render_settings_page() {
		?>
		<div class="wrap">
			<h1>Block Library Settings</h1>
			<form method="post" action="options.php">
				<?php
				settings_fields('block_library_settings');
				do_settings_sections('block-library-settings');
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	public function render_admin_page() {
		$this->render_block_library();
	}

	public function handle_manual_gitlab_download() {
		if (!current_user_can('manage_options')) {
			wp_send_json_error(['message' => 'Unauthorized']);
		}

		$git_getter = new Imswp_GitGetter($this->plugin_name, $this->version);
		$result = $git_getter->download_gitlab_repository_zip();

		if ($result['success']) {
			wp_send_json_success(['message' => 'Repositories downloaded successfully', 'updated_themes' => $result['updated_themes']]);
		} else {
			wp_send_json_error(['message' => 'Failed to download repositories']);
		}
	}

}
