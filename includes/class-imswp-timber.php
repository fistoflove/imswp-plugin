<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */

 use Timber\Timber;
 use Timber\Menu;
/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */
class Imswp_Timber {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->set_timber_options();
		$this->init_timber();
		add_filter('timber/twig', [ $this, 'add_to_twig' ] );
		add_filter('timber/context', [ $this, 'add_to_timber_menus' ], 5 );
	}

	public function init_timber() {

	}

	public static function set_timber_options() {
		Timber::$dirname = [
			get_template_directory() . '/templates',
			get_template_directory() . '/core',
			plugin_dir_path( __DIR__ ) . 'admin/templates',
			plugin_dir_path( __DIR__ )
		];
	}

    public static function theme_menus() {
        $menus = [
            'header-primary-menu' => __('Header Primary Menu'),
            'header-secondary-menu' => __('Header Secondary Menu'),
            'footer-primary-menu' => __('Footer Primary Menu'),
            'footer-secondary-menu' => __('Footer Secondary Menu')
        ];
        return $menus;
    }

	public static function add_to_timber_menus($context) {
		$menus = IMSWP\Helper\Helper::theme_menus();
		foreach($menus as $k => $v) {
		  $context[str_replace('-', '_', $k)]  = new Menu($k);
		}
		return $context;
	}

	public function add_to_twig($twig) {
		$twig->addFunction(new Twig_Function('yebote', [ $this, 'yebote' ]));
		$twig->addFunction(new Twig_Function('translateString', [ $this, 'translateString' ]));
		$twig->addFunction(new Twig_Function('local_image', [ $this, 'local_image' ]));
		$twig->addFunction(new Twig_Function('local_file', [ $this, 'local_file' ]));
		$twig->addFunction(new Twig_Function('local_child_image', [ $this, 'local_child_image' ]));
		$twig->addFunction(new Twig_Function('imageElement', [ $this, 'imageElement' ]));
		$twig->addFunction(new Twig_Function('blockSection', [ $this, 'blockSection' ]));
		$twig->addFunction(new Twig_Function('blockClasses', [ $this, 'blockClasses' ]));
		$twig->addFunction(new Twig_Function('output_css', [ $this, 'output_css' ]));
		$twig->addFunction(new Twig_Function('imageFromObject', [ $this, 'imageFromObject' ]));
		return $twig;
	}

	public function blockClasses($block) {
		$output = substr($block['name'], 4) . ' ';
		$output .= (isset($block['className']) ? $block['className'] . ' ' : '');
		return $output;
	}

	public function blockSection($block) {
		$output = (isset($block['anchor']) ? 'id="'. $block['anchor'] .'"' : '');
		$output .= 'data-block="'. substr($block['name'], 4) .'"';
		return $output;
	}

	public function imageElement($image, $size = false, $lazy = true, $cdn = true, $cdn_params = []) {
		$data = [
		  "image" => $image,
		  "size"  => $size,
		  "lazy"  => $lazy,
		  "cdn"   => $cdn,
		  "cdn_params"   => $cdn_params
		];
  
		Timber::render( 'image.twig', $data );
	}

	public function output_css($block) {
		if(file_exists($block['path'] . '/style.min.css')) {
		  $styles = file_get_contents($block['path'] . '/style.min.css');
		  return $styles;
		} elseif(file_exists($block['path'] . '/compiled.css')) {
		  $styles = file_get_contents($block['path'] . '/compiled.css');
		  return $styles;
		}
	}

	public function yebote($thing) {
		$key = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 5);
		$script = "<script>document.getElementById('" . $key . "-json').innerHTML = JSON.stringify(" . json_encode($thing) . ", undefined, 2);</script>";
		$container = "<pre id='" . $key . "-json'></pre>";
		return $container . $script;
	}

	public static function imageFromObject($params) {
		if(array_key_exists('image', $params)) {
			$baseURL = "";

			$cdn_token = get_option('image_cdn_token');
	
			if($cdn_token) {
				$baseURL = "https://" . $cdn_token . ".cloudimg.io/v7/";
			}

			if(array_key_exists('breakpoints', $params)) {
				$breakpoints = [];

				foreach($params['breakpoints'] as $key => $val) {
					$breakpoints["(min-width: " . $key . "px)"] = $val;
				}
			} else {
				$breakpoints = [
					"(min-width: 1921px)" => 900,
					"(min-width: 1280px)" => 600,
					"(min-width: 1024px)" => 500,
					"(min-width: 500px)" => 768,
					"(min-width: 401px)" => 450,
				];
			}

			if(array_key_exists('default_width', $params)) {
				$default_width = $params['default_width'];
			} else {
				$default_width = 400;
			}

			if(array_key_exists('loading', $params)) {
				$loading = $params['loading'];
			} else {
				$loading = 'lazy';
			}

			$picture = '<picture>';
		
			foreach ($breakpoints as $media => $width) {
				$srcset = $baseURL . $params['image']['url'] . '?func=fit&w=' . $width;
				$picture .= '<source media="' . $media . '" srcset="' . $srcset . '">';
			}
		
			$defaultSrcset = $baseURL . $params['image']['url'] . '?func=fit&w=' . $default_width;

			$picture .= '<source srcset="' . $defaultSrcset . '">';
		
			$imgSrc = $baseURL . $params['image']['url'] . '?func=fit&w=900';
			
			$picture .= '<img src="' . $imgSrc . '" alt="Example Image" loading="' . $loading . '" height="' . $params['image']['height'] . '" width="' . $params['image']['width'] . '">';
		
			$picture .= '</picture>';
		
			return $picture;
		}
	}
}
