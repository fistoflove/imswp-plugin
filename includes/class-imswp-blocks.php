<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */

 use Timber\Timber;

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */

class Imswp_Blocks {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->init_blocks();
	}

	public function init_blocks() {

	}

    public function get_blocks_places() {
        $valid_places = [];
        
        $raw_places = [get_stylesheet_directory() . '/blocks'];

        if(is_child_theme()) {
            array_push($raw_places, get_template_directory() . '/blocks');
        }
          
        foreach($raw_places as $place) {
            if(is_dir($place)) {
                $valid_places[] = $place;
            }
        }
        return $valid_places;
    }

    public function get_blocks() {
        $places = $this->get_blocks_places();

        $blocks = [];

        foreach($places as $path) {
            foreach (new \DirectoryIterator( $path ) as $file) {
                if ($file->isDot()) continue;
            
                if ($file->isDir()) {
                    $blocks[] = $file->getPathname();
                }
            }
        }
        return $blocks;
    }

    public function register_block_fields($block) {
        if(file_exists($block . '/fields.php')) {
            require_once $block . '/fields.php';
        }
    }

    public function register_blocks() {

        $blocks = $this->get_blocks();

        foreach($blocks as $block) {

            register_block_type( $block );

            $this->register_block_fields( $block );

//            $this->compile_block_style( $block );
        }
    }
}
