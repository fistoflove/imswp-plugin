<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */
namespace IMSWP\Helper;

use Timber\Timber;

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */

class Helper {

    protected $block;
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct() {
		$this->init_helper();
	}

	public function init_helper() {

	}

	public static function sayHello() {
		echo "hello";
	}
    public static function output_this_block_css($block) {
        // if(file_exists(get_stylesheet_directory() . '/blocks/' . $block . '/style.min.css')) {
        //     $minified_css = file_get_contents(get_stylesheet_directory() . '/blocks/' . $block . '/style.min.css');
        //     echo "<style>" . $minified_css . "</style>";
        // } elseif(file_exists(get_stylesheet_directory() . '/blocks/' . $block . '/compiled.css')) {
        //     $minified_css = file_get_contents(get_stylesheet_directory() . '/blocks/' . $block . '/compiled.css');
        //     echo "<style>" . $minified_css . "</style>";
        // }
    }

    public static function theme_menus() {
        $menus = [
            'header-primary-menu' => __('Header Primary Menu'),
            'header-secondary-menu' => __('Header Secondary Menu'),
            'footer-primary-menu' => __('Footer Primary Menu'),
            'footer-secondary-menu' => __('Footer Secondary Menu')
        ];
		return $menus;
    }

	public static function imageFromObject($params) {
		if(array_key_exists('image', $params)) {
			$baseURL = "";

			$cdn_token = get_option('image_cdn_token');
	
			if($cdn_token) {
				$baseURL = "https://" . $cdn_token . ".cloudimg.io/v7/";
			}

			if(array_key_exists('breakpoints', $params)) {
				$breakpoints = [];

				foreach($params['breakpoints'] as $key => $val) {
					$breakpoints["(min-width: " . $key . "px)"] = $val;
				}
			} else {
				$breakpoints = [
					"(min-width: 1921px)" => 900,
					"(min-width: 1280px)" => 600,
					"(min-width: 1024px)" => 500,
					"(min-width: 500px)" => 768,
					"(min-width: 401px)" => 450,
				];
			}

			if(array_key_exists('default_width', $params)) {
				$default_width = $params['default_width'];
			} else {
				$default_width = 400;
			}

			if(array_key_exists('loading', $params)) {
				$loading = $params['loading'];
			} else {
				$loading = 'lazy';
			}

			$picture = '<picture>';
		
			foreach ($breakpoints as $media => $width) {
				$srcset = $baseURL . $params['image']['url'] . '?func=fit&w=' . $width;
				$picture .= '<source media="' . $media . '" srcset="' . $srcset . '">';
			}
		
			$defaultSrcset = $baseURL . $params['image']['url'] . '?func=fit&w=' . $default_width;

			$picture .= '<source srcset="' . $defaultSrcset . '">';
		
			$imgSrc = $baseURL . $params['image']['url'] . '?func=fit&w=900';
			
			$picture .= '<img src="' . $imgSrc . '" alt="Example Image" loading="' . $loading . '" height="' . $params['image']['height'] . '" width="' . $params['image']['width'] . '">';
		
			$picture .= '</picture>';
		
			echo $picture;
		}
	}
}
