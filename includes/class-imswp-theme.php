<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */

 use Timber\Timber;

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */
class Imswp_Theme {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->register_theme_menus();
	}

	public function theme_favicon() {
		if(file_exists(get_template_directory() . '/core/favicon.twig')) {
			Timber::render( 'core/favicon.twig');
		}
	}

	public function theme_option_pages() {
		add_menu_page(
			'Theme Settings',
			'Theme Settings',
			'manage_options',
			'theme-settings',
			[$this, 'render_theme_settings_page'], 
			'dashicons-admin-generic', 
			80
		);
	}

	public function render_theme_settings_page() {
	//	Timber::render( 'admin.twig');
	// check user capabilities
	if (!current_user_can('manage_options')) {
		return;
	  }
	
	  if (isset($_GET['settings-updated'])) {
		// add settings saved message with the class of "updated"
		add_settings_error('my_options_messages', 'my_options_message', __('Settings Saved', 'my_options'), 'updated');
	  }
	
	  // show error/update messages
	  settings_errors('my_options_messages');
	  ?>
  
	  <div class="wrap">
		<h1><?php echo esc_html(get_admin_page_title()); ?></h1>
		<form action="options.php" method="post">
		  <?php
		  // output security fields for the registered setting "image_cdn_token"
		  settings_fields('image_cdn_token');
		  // output setting sections and their fields
		  do_settings_sections('image_cdn_token');
		  // output save settings button
		  submit_button('Save Settings');
		  ?>
		</form>
	  </div>
	  <?php
	}

	public function register_theme_menus() {
        $menus = IMSWP\Helper\Helper::theme_menus();
		register_nav_menus($menus);
    }

	public function register_image_cdn_field() {
		// register a new setting for "my_options" page
		register_setting('image_cdn_token', 'image_cdn_token');

		// register a new section in the "my_options" page
		add_settings_section(
		  'image_cdn_token_section',
		  'Performance',
		  [$this, 'performance_section_cb'], 
		  'image_cdn_token'
		);

		// register a new field in the "my_options_section" section, inside the "my_options" page
		add_settings_field(
		  'image_cdn_token_field', 
		  'CloudImage CDN Token', 
		  [$this, 'image_cdn_token_field_cb'], 
		  'image_cdn_token', 
		  'image_cdn_token_section'
		);
	}

	function performance_section_cb() {
//		echo '<p>Section description...</p>';
	}

	function image_cdn_token_field_cb() {
		// get the value of the setting we've registered with register_setting()
		$setting = get_option('image_cdn_token');
		// output the field
		?>
		<input type="text" name="image_cdn_token" value="<?php echo isset($setting) ? esc_attr($setting) : ''; ?>">
		<?php
	}
}
