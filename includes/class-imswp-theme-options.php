<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */

class Imswp_ThemeOptions {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	public function set_default_options() {
		if(!get_option('theme_head_code')) {
			add_option('theme_head_code', array());
		}
	}
}
