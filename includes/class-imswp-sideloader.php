<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://imsmarketing.ie/
 * @since      1.0.0
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 */

 use Timber\Timber;

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Imswp
 * @subpackage Imswp/includes
 * @author     IMS Marketing <dev@imsmarketing.ie>
 */

class Imswp_SideLoader {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->init();
	}

	public function init() {
		add_action('init', [$this, 'register_sideloaded_blocks']);
	}

	public function register_sideloaded_blocks() {
		if (!get_option('block_sideload_enabled', false)) {
			return;
		}

		$themes_dir = get_theme_root();
		$theme_dirs = scandir($themes_dir);
		$active_theme = wp_get_theme()->get_stylesheet(); // Get the active theme's directory name

		// Retrieve a list of all blocks from the active theme
		$active_theme_blocks = $this->get_blocks_from_directory(get_stylesheet_directory() . '/blocks');

		foreach ($theme_dirs as $theme_dir) {
			if ($theme_dir !== '.' && $theme_dir !== '..' && $theme_dir !== $active_theme) {
				$blocks_dir = $themes_dir . '/' . $theme_dir . '/blocks';
				if (is_dir($blocks_dir)) {
					$this->register_blocks_from_directory($blocks_dir, $active_theme_blocks);
				}
			}
		}
	}

	private function get_blocks_from_directory($blocks_dir) {
		$blocks = [];
		if (is_dir($blocks_dir)) {
			$block_dirs = scandir($blocks_dir);
			foreach ($block_dirs as $block_dir) {
				if ($block_dir !== '.' && $block_dir !== '..') {
					$block_json_path = $blocks_dir . '/' . $block_dir . '/block.json';
					if (file_exists($block_json_path)) {
						$block_data = json_decode(file_get_contents($block_json_path), true);
						$blocks[] = $block_data['name'];
					}
				}
			}
		}
		return $blocks;
	}

	private function register_blocks_from_directory($blocks_dir, $active_theme_blocks) {
		$block_dirs = scandir($blocks_dir);
		foreach ($block_dirs as $block_dir) {
			if ($block_dir !== '.' && $block_dir !== '..') {
				$block_path = $blocks_dir . '/' . $block_dir;
				$block_json_path = $block_path . '/block.json';
				if (file_exists($block_json_path)) {
					$block_data = json_decode(file_get_contents($block_json_path), true);
					$block_name = $block_data['name'];
					$block_registry = WP_Block_Type_Registry::get_instance();

					// Check if the block is already registered or exists in the active theme
					if (!$block_registry->is_registered($block_name) && !in_array($block_name, $active_theme_blocks)) {
						register_block_type($block_json_path);
						$this->register_block_fields($block_path);
					} else {
						error_log("Block type \"$block_name\" is already registered or exists in the active theme.");
					}
				}
			}
		}
	}

	private function register_block_fields($block_path) {
		if (file_exists($block_path . '/fields.php')) {
			require_once $block_path . '/fields.php';
		}
	}
}
