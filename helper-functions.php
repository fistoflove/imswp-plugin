<?php

function blockClasses($block) {
    $output = substr($block['name'], 4) . ' ';
    $output .= (isset($block['className']) ? $block['className'] . ' ' : '');
    return $output;
}

function blockSection($block) {
    $output = (isset($block['anchor']) ? 'id="'. $block['anchor'] .'"' : '');
    $output .= 'data-block-name="'. substr($block['name'], 4) .'"';
    return $output;
}